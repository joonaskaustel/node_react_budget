const icons = {
  entertainment: 'fa-theater-masks',
  transport: 'fa-car',
  food: 'fa-utensils',
  shopping: 'fa-shopping-bag',
}

export default icons;