import React, { Component } from "react";
import axios from "axios";

import getIcon from '../utils/getFontAwesome'

class history extends Component {
  constructor() {
    super();
    this.state = {
      historyItems: []
    };
  }

  componentDidMount() {
    axios.get("/items").then(res => {
      const data = res.data;
      this.setState({
        historyItems: data
      });
    });
  }

  render() {
    const items = this.state.historyItems.map(item => {
      return (
        <div key={item.id} className="box">
          <nav className="level">
            <span className="level-left"><i className={"fas " + getIcon[item.category]}></i>&nbsp;{item.category}</span>
            <span
              className={
                "level-right has-text-" +
                (item.type === "expense" ? "danger" : "success")
              }
            >
              {item.type === "expense" ? "-" : "+"} €{item.amount}
            </span>
          </nav>
        </div>
      );
    });

    return (
      <div className="columns">
        <div className="column">{items}</div>
      </div>
    );
  }
}

export default history;
