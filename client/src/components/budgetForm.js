import React, { Component } from 'react'
import axios from 'axios'

class form extends Component {

  constructor() {
    super()
    this.state = {
      amount: '',
      category: '-',
      type: 'expense',
      error: ''
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange(e) {    
    const {name, value} = e.target;
    this.setState({
      [name]: value
    })
  }

  handleSubmit(e) {
    
    const data = {
      amount: this.state.amount.trim(),
      category: this.state.category,
      type: this.state.type
    }
    
    if (data.amount === '0' || data.amount === '') {
      this.setState({
        error: 'amount'
      });
      e.preventDefault();
      return;
    }
    
    axios.post('/add-item', { data })
    .then(res => {
      console.log(res);
      this.setState({
        amount: '',
        category: '-',
        type:'expense'
      })
    })   
  }

  render() {
    return (
      <div className='columns'>
        <div className="column is-full">
          <form onSubmit={this.handleSubmit}>

            <div className="buttons has-addons">
              <button type='button' className={this.state.type === 'expense' ? 'button is-success' : 'button'} name='type' value='expense' onClick={this.handleChange}>Expense</button>
              <button type='button' className={this.state.type === 'income' ? 'button is-success' : 'button'} name='type' value='income' onClick={this.handleChange}>Income</button>
            </div>

            <div className="field is-grouped">
            <div className="control ">
              <div className="select">
                <select name="category" id="" value={this.state.category} onChange={this.handleChange}>
                  <option value="-" disabled hidden>Choose category</option>
                  <option value="food">food</option>
                  <option value="entertainment">entertainment</option>
                  <option value="transport">transport</option>
                  <option value="shopping">shopping</option>
                </select>
              </div>
            </div>

              <div className="control is-expanded">
                <input
                  className={'input ' + (this.state.error === 'amount' ? 'is-danger' : '')}
                  type="text"
                  name="amount"
                  value={this.state.amount}
                  placeholder="amount"
                  onChange={this.handleChange} />
              </div>

              <div className="control">
                <button
                  type="submit"
                  className="button is-link">
                  Add
                </button>
              </div>
            </div>

          </form>
        </div>
      </div>
    )
  }
}

export default form;