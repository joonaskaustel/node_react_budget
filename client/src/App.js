import React, { Component } from 'react';

import Form from './components/budgetForm';
import History from './components/itemHistory';

import './App.css';
import 'bulma/css/bulma.css';

class App extends Component {
  render() {
    return (
      <div className="container is-fluid">
        <Form/>
        <History/>
      </div>
    );
  }
}

export default App;
