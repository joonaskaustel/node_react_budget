const express = require('express')
const bodyparser = require('body-parser')
const app = express()
const db = require('./utils/db')

app.use(bodyparser.json());

app.get('/items', (req, res) => {
  db.execute('SELECT * FROM items ORDER BY id DESC')
    .then(result => {
      res.json(result[0])
    })
    .catch(err => {
      console.log(err);
    });
})

app.post('/add-item', (req, res) => {
  const data = req.body.data  
  db.execute('INSERT INTO items (amount, category, type) VALUES (?, ?, ?)',
  [data.amount, data.category, data.type])
  .then(result => {
    res.json(result[0])
  })
  .catch(err => {
    console.log(err);
  });
})

const port = 5000

app.listen(port, () => 
  console.log(`server started on ${port}`)
)